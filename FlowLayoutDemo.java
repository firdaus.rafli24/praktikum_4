import java.awt.*;

public class FlowLayoutDemo extends Frame{

    public FlowLayoutDemo(){

    }

    public static void main(String[] args) {
        
        FlowLayoutDemo fld = new FlowLayoutDemo();
        fld.setLayout(new FlowLayout(FlowLayout.CENTER,10,10));
        fld.add(new Button("Press Me"));
        fld.add(new Button("Don't Press Me"));
        fld.setSize(300,200);
        fld.setVisible(true);

    }
}