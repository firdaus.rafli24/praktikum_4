import java.awt.*;

public class ComplexLayout extends Frame{

    public ComplexLayout(){

    }

    public static void main(String[] args) {
        
        ComplexLayout cl = new ComplexLayout();
        Panel panelNorth = new Panel();
        Panel panelCenter = new Panel();
        Panel panelWest = new Panel();

        panelNorth.add(new Button("File"));
        panelNorth.add(new Button("Help"));

        panelCenter.setLayout(new GridLayout(4,4));
        panelCenter.add(new TextField("Work space Region"));

        panelWest.add(new Button("West"));

        cl.add(panelNorth,BorderLayout.NORTH);
        cl.add(panelCenter,BorderLayout.CENTER);
        cl.add(panelWest,BorderLayout.WEST);
        cl.setSize(300,300);
        cl.setVisible(true);

    }

}