/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tipcalculator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.Locale;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;

/**
 *
 * @author Rafli
 */
public class TipCalculatorController{
    
    private static final Locale locale = new Locale("en", "US");
    private static final NumberFormat currency = NumberFormat.getCurrencyInstance(locale);
    private static final NumberFormat percent = NumberFormat.getPercentInstance();
    private static final BigDecimal HUNDRED = BigDecimal.valueOf(100);
    
    @FXML
    private Label amountLabel;
    @FXML
    private Label percentageLabel;
    @FXML
    private Label tipLabel;
    @FXML
    private Label totalLabel;
    @FXML
    private TextField amountValue;
    @FXML
    private TextField tipValue;
    @FXML
    private TextField totalValue;
    @FXML
    private Slider percentageSlider;
    
    
    public void update(){
        BigDecimal amount;
        try{
            amount = new BigDecimal(amountValue.getText());
        }catch(NumberFormatException ex){
            tipValue.setText("Invalid Input");
            totalValue.setText("Invalid input");
            return;
        }
        
        BigDecimal tipFactor = BigDecimal.valueOf(percentageSlider.getValue()).divide(HUNDRED,2,RoundingMode.HALF_UP);
        BigDecimal tip = amount.multiply(tipFactor);
        BigDecimal total = amount.add(tip);
        tipValue.setText(currency.format(tip));
        totalValue.setText(currency.format(total));
    }
    
    public void initialize() {
        // TODO
        percentageLabel.textProperty().bind(percentageSlider.valueProperty().asString("%.0f"));
        currency.setRoundingMode(RoundingMode.HALF_UP);
        percentageSlider.valueProperty().addListener(o ->update());
        amountValue.textProperty().addListener(o->update());
        
    }    
    
}
