/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FirstPackage;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 *
 * @author Rafli
 */
public class FirstClass extends Application {
    
    @Override
    public void start(Stage primaryStage) {
        BorderPane p = new BorderPane();
        Text t = new Text("Hello FX");
        t.setFont(Font.font("Arial", 60));
        p.setCenter(t);
        primaryStage.setScene(new Scene(p));
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
