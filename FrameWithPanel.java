import java.awt.*;

public class FrameWithPanel{

    private Frame f;

    public FrameWithPanel(String title){

        f = new Frame(title);

    }

    public void launchFrame(){

        f.setSize(200,200);
        f.setBackground(Color.blue);
        f.setLayout(null); //Override default layout mgr


        Panel pan = new Panel();
        pan.setSize(100, 100);
        pan.setBackground(Color.yellow);

        Panel pan1 = new Panel();
        pan1.setSize(200, 200);
        pan1.setBackground(Color.red);

        Panel pan2 = new Panel();
        pan2.setSize(300, 300);
        pan2.setBackground(Color.blue);

        Panel pan3 = new Panel();
        pan3.setSize(400, 400);
        pan3.setBackground(Color.orange);

        f.add(pan);
        f.setVisible(true);
        f.add(pan1);
        f.setVisible(true);
        f.add(pan2);
        f.setVisible(true);
        f.add(pan3);
        f.setVisible(true);

    }
    
    public static void main(String[] args) {
        
        FrameWithPanel guiWindow = new FrameWithPanel("Frame With Panel");
        guiWindow.launchFrame();

    }

}